<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['user_id'])) {
    $usuario_id = $_SESSION['user_id'];
} else {
    header('Location: login.php');
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$status = $_GET['status'];
// $tipo = $_GET['tipo'];


$where = "WHERE status = $status ";
if (!empty($data1)) {
    $where .= "AND data BETWEEN '$data1' AND '$data2'";
    // if ($tipo != "") {
    //     $where .= " AND c.status = $tipo";
    // }
} else {
    // if ($tipo != "") {
    //     $where = "where c.status = $tipo";
    // }
}

$sql = "SELECT 
			* 
		FROM 
			chamado
        WHERE status = $status
		";
$res = mysqli_query($conn, $sql);

$sql = "SELECT  
            SUM( TIME_TO_SEC(tempo) ) AS timeSum, semana
        FROM chamado 
        $where
        GROUP BY semana";
$res_soma_semana = mysqli_query($conn, $sql);

$sql = "SELECT  
            SUM( TIME_TO_SEC(tempo) )  AS timeSum, semana
        FROM chamado
        $where";
$res_total_soma = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_total_soma)) {
    $total_soma = $row[0];
}

//transforma em minutos
$total_soma /= 60;
$min_restante_total_soma = fmod($total_soma, 60);
$total_soma /= 60;
$total_soma = intval($total_soma);

//2020-11-01 2020-11-30
$sql = "SELECT 
            *
        FROM chamado
        $where";
$res = mysqli_query($conn, $sql);


?>

<div class="table-responsive">
    <table class="table table-bordered" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th colspan="4" class="text-center"><b>Tempo total: </b> <?= $total_soma ?>h e <?= $min_restante_total_soma ?>min</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <?php while ($row = mysqli_fetch_array($res_soma_semana)) {
                    $row['timeSum'] /= 60;
                    $min_restante_time_sum = fmod($row['timeSum'], 60);
                    $row['timeSum'] /= 60;
                    $row['timeSum'] = intval($row['timeSum']);
                ?>
                    <th><b>Semana <?= $row['semana'] ?>:</b> <?= $row['timeSum'] ?>h e <?= $min_restante_time_sum ?>min</th>
                <?php } ?>
            </tr>
        </tfoot>
    </table>
</div>
<div class="table-responsive">
    <table class="table table-bordered" id="dataTableChamado" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ID do Chamado</th>
                <th>Titulo</th>
                <th>Data</th>
                <th>Tempo</th>
                <th>Semana</th>
                <th width="40%">Descrição</th>
                <th width="10%">Editar</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID do Chamado</th>
                <th>Titulo</th>
                <th>Data</th>
                <th>Tempo</th>
                <th>Semana</th>
                <th width="40%">Descrição</th>
                <th width="10%">Editar</th>
            </tr>
        </tfoot>
        <tbody>
            <?php
            while ($row = mysqli_fetch_array($res)) {
                if ($row['id_chamado'] == "NULL") {
                    $id_chamado = "Sem informação";
                } else {
                    $id_chamado = $row['id_chamado'];
                }
            ?>
                <tr>
                    <td><?= $id_chamado ?></td>
                    <td><?= $row['titulo'] ?></td>
                    <td><?= date('d/m/Y', strtotime($row['data'])) ?></td>
                    <td><?= $row['tempo'] ?></td>
                    <td><?= "Semana" . $row['semana'] ?></td>
                    <td><?= $row['descricao']; ?></td>
                    <td>
                        <center>
                            <button class="btn btn-warning btn-circle" onclick="edit_chamado(<?= $row['id'] ?>)">
                                <i class="fas fa-edit"></i>
                            </button>
                        </center>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>