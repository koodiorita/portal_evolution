<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

$id = $_GET['id'];

$sql = "select * from cliente where id = $id";

$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $cnpj			= $row['cnpj'];
    $razao			= $row['razao_social'];
    $ie 			= $row['ie'];
    $email 			= $row['email'];
    $responsavel	= $row['responsavel'];
    $endereco		= $row['endereco'];
    $numero 		= $row['numero'];
    $bairro 		= $row['bairro'];
    $cidade 		= $row['cidade'];
    $cep    		= $row['cep'];
    $telefone		= $row['telefone'];
    $telefone2		= $row['telefone2'];
    $whatsapp		= $row['whatsapp'];
    $dominio		= $row['dominio'];
    $observacao		= $row['observacao'];
}

?>
<br>
<form action="php/edita_cliente.php" method="POST"  >
    <input type="hidden" id="id_cli_edit" name="id_cli_edit" value="<?php echo $id;?>">
    <div class="form-row">
        <div class="col">
            <input name="cnpj_cli_edit" type="text" placeholder="CNPJ da empresa" class="form-control" value="<?php echo $cnpj;?>" required /><br>
        </div>
        <div class="col">
            <input name="ie_cli_edit" type="text" placeholder="Incrição Estadual" class="form-control" value="<?php echo $ie;?>" /><br>
        </div>
    </div>
        
    <div class="form-row">
        <div class="col">
            <input name="razao_cli_edit" type="text" placeholder="Razão Social" class="form-control" value="<?php echo $razao;?>" required/><br>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <input name="email_cli_edit" type="text" placeholder="Email" class="form-control" value="<?php echo $email;?>" /><br>
        </div>
        <div class="col">
            <input name="responsavel_cli_edit" type="text" placeholder="Responsavel" class="form-control" value="<?php echo $responsavel;?>" required/><br>
        </div>
    </div>
    <div class="form-row">
        <div class="col-4">
            <input id="cep" name="cep_cli_edit" type="text" placeholder="CEP" class="form-control" value="<?php echo $cep;?>" required/><br>
        </div>
        <div class="col">
            <input value="Buscar Cep" id="buscaCepCliEdit" type="button" placeholder="Cep" class="btn btn-primary" /><br>
        </div>
    </div>
    <div class="form-row">
        <div class="col-8">
            <input id="endereco_cli_edit" name="endereco_cli_edit" type="text" placeholder="Endereço" value="<?php echo $endereco;?>" class="form-control" required/><br>
        </div>
        <div class="col-4">
            <input name="numero_cli_edit" type="text" placeholder="Numero" class="form-control" value="<?php echo $numero;?>" required/><br>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col">
            <input id="bairro_cli_edit" name="bairro_cli_edit" type="text" placeholder="Bairro" class="form-control" value="<?php echo $bairro;?>" required /><br>
        </div>
        <div class="col">
            <input id="cidade_cli_edit" name="cidade_cli_edit" type="text" placeholder="Cidade" class="form-control" value="<?php echo $cidade;?>" required /><br>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <input name="telefone_cli_edit" type="text" placeholder="Telefone" class="form-control" value="<?php echo $telefone;?>" required/>
        </div>
        <div class="col">
            <input name="telefone2_cli_edit" type="text" placeholder="Telefone 2 (Opcional)" class="form-control"  value="<?php echo $telefone2;?>" />
        </div>
        <div class="col">
            <input name="whatsapp_cli_edit" type="text" placeholder="WhatsApp" class="form-control" value="<?php echo $whatsapp;?>" /><br>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <input name="dominio_cli_edit" type="text" placeholder="Ex: www.evolutionsoft.com.br" class="form-control" value="<?php echo $dominio;?>" />
        </div>
    </div></br>         
    <textarea name="observacao_cli_edit" type="text" placeholder="Digite uma observação" class="form-control" value="<?php echo $observacao;?>" ></textarea><br>
    
    
    <button class="btn btn-success" type="submit" style="float: right">Alterar</button>
</form>

<script>
        $(document).ready(function () {
            $("#buscaCepCliEdit").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_cli_edit").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_cli_edit").val(dados.logradouro);
                            $("#endereco_cli_edit").css("background","#eee");
                            $("#bairro_cli_edit").val(dados.bairro);
                            $("#bairro_cli_edit").css("background","#eee");
                            $("#cidade_cli_edit").val(dados.localidade);
                            $("#cidade_cli_edit").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });
        </script>