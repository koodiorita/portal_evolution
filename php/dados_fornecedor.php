<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

$id = $_GET['id'];

$sql = "select * from fornecedor where id = $id";

$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $nome = $row['nome'];
    $cnpj = $row['cnpj'];
    $endereco = $row['endereco'];
    $numero = $row['numero'];
    $bairro = $row['bairro'];
    $cidade = $row['cidade'];
    $cep = $row['cep'];
    $telefone = $row['telefone'];
    $telefone2 = $row['telefone2'];

}

?>
<br>
<form action="php/edita_fornecedor.php" method="POST"  >
                            <input type="hidden" id="id_for_edit" name="id_for_edit" value="<?= $id ?>" >
                        <div class="form-row">
                            <div class="col">
                                <input name="nome_for_edit" id="nome_for_edit" type="text" placeholder="Nome da Fornecedor" class="form-control" value="<?= $nome ?>" required /><br>
                            </div>
                            <div class="col">
                                <input name="cnpj_for_edit" id="cnpj_for_edit" type="text" placeholder="CPF / CNPJ" class="form-control" value="<?= $cnpj ?>" /><br>
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep_for_edit" name="cep_for_edit" type="text" placeholder="CEP" class="form-control" value="<?= $cep ?>" required /><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCepFor_edit" type="button"  class="btn btn-primary" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco_for_edit" name="endereco_for_edit" type="text" placeholder="Endereço completo" class="form-control" 
                                value="<?= $endereco ?>" required/><br>
                            </div>
                            <div class="col-4">
                                <input id="numero_for_edit" name="numero_for_edit" type="text" placeholder="Numero" class="form-control" value="<?= $numero ?>" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro_for_edit" name="bairro_for_edit" type="text" placeholder="Bairro" class="form-control" value="<?= $bairro ?>" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade_for_edit" name="cidade_for_edit" type="text" placeholder="Cidade" class="form-control" value="<?= $cidade ?>" required /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone_for_edit" id="telefone_for_edit" type="text" placeholder="Telefone" class="form-control" value="<?= $telefone ?>" required />
                            </div>
                            <div class="col">
                                <input name="telefone2_for_edit" id="telefone2_for_edit" type="text" placeholder="Telefone 2 (Opcional)" class="form-control" 
                                value="<?= $telefone2 ?>" /></br>
                            </div>
                            
                        </div>
                                
                        
                        <button class="btn btn-success" type="submit" style="float: right">Alterar</button>
                        <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
</form>

<script>
        $(document).ready(function () {
            $("#buscaCepCliEdit").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_cli_edit").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_cli_edit").val(dados.logradouro);
                            $("#endereco_cli_edit").css("background","#eee");
                            $("#bairro_cli_edit").val(dados.bairro);
                            $("#bairro_cli_edit").css("background","#eee");
                            $("#cidade_cli_edit").val(dados.localidade);
                            $("#cidade_cli_edit").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });
        </script>