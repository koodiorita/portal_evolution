<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$fornecedor = $_GET['fornecedor'];


$where="";
if (!empty($data1)) {
    $where = "where c.vencimento between '$data1' AND '$data2'";
    if (!empty($fornecedor)) {
        $where .= " AND f.nome = '$fornecedor'";
    }
}else{
    if (!empty($fornecedor)) {
        $where = "where f.nome = '$fornecedor'";
    }

}

$sql = "SELECT 
c.id,
c.id_fornecedor,
f.nome,
c.valor,
c.vencimento,
c.descricao,
c.status
FROM 
`contas_pagar` as c
inner join fornecedor as f ON
c.id_fornecedor = f.id
$where";

$res = mysqli_query($conn,$sql);

$valor_pendente = 0;  
$valor_sucess = 0;  
$valor_cancel = 0;  
?>
<table class="table table-bordered" id="tableFiltroPagar">
    <thead>
      <tr>
          <th>Vencimento</th>
          <th>Valor</th>
          <th>Fornecedor</th>
          <th>Descricao</th>
          <th width="10%">Pagar</th>
          <th width="10%">Anular</th>
        </tr>
    </thead>
    <tbody>
		<?php
			
		while($row = mysqli_fetch_array($res)) { 
      $responsavel = $row['nome'];
      $valor = $row['valor'];
			?>
	   <tr>
                <td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>
                                <td><?php echo "R$ ".number_format($row['valor'], 2, '.', '');?></td>
                                <td><?php echo $responsavel;?></td>
                                <td><?php echo $row['descricao'];?></td>

                <?php 
                
                if ($row['status']==0) {
                $valor_pendente += $valor;
                }elseif ($row['status']==1) {
                  $valor_sucess += $valor;
                }else{
                  $valor_cancel += $valor;
                }

                if($row['status'] == 0) { ?>

                <td>
                  <center>
                    <button class="btn btn-primary btn-circle" onclick="pagar(<?php echo $row['id'];?>)" ><i class="fas fa-download" ></i></button>
                  </center></td>
                <td>
                  <center>
                    <button class="btn btn-danger btn-circle" onclick="cancel(<?php echo $row['id'];?>)" ><i class="fas fa-window-close" ></i></button>
                  </center>
                </td>

                <?php }elseif($row['status']==1){ ?>

                <td colspan="2" style="text-align: center;">Recebido</td>

                <?php }else{?>
                <td colspan="2" style="text-align: center;">Cancelado</td>


                <?php } ?>

              </tr>
						<?php }?>	
      </tbody>
      <tfoot>
        <tr>
          <th>Vencimento</th>
          <th>Valor</th>
          <th>Fornecedor</th>
          <th>Descricao</th>
           <th width="10%">Pagar</th>
           <th width="10%">Anular</th>
        </tr>
      </tfoot>
</table>
<br>
<table border="1" class="informacoes">
    <tr>
        <th style="color:yellow;">Valor Pendente: <?= "R$ ".number_format($valor_pendente,2,".",""); ?></th>
        <th style="color: #32CD32;">Valor Recebido: <?= "R$ ".number_format($valor_sucess,2,".",""); ?></th>
        <th style="color:red;">Valor Cancelado: <?= "R$ ".number_format($valor_cancel,2,".",""); ?></th>
    </tr>
</table>

<script>

  $(document).ready(function() {
      $('#tableFiltroPagar').DataTable( {
      });
  });

  function cancel(id_receber){
    var resp = confirm("Deseja anular essa conta ?");
    if(resp == true){
      $.get( "php/anular_conta.php?id_receber="+id_receber, function( data ) {
          location.reload();
      });
    }
  }
      </script>
