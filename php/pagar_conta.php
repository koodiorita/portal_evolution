<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getValorCaixa(){
    global $conn;

    $sql = "select valor from caixa";

    $res= mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $caixa = $row['valor'];
    }


    return $caixa;
}
function getUser($id_user){
    global $conn;
    $sql = "select * from user where id = $id_user";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $user = $row['usuario'];
    }

    return $user;
}

if(!empty($_SESSION['user_id'])){
    $usuario_id = $_SESSION['user_id'];
  }else{
      header('Location: login.php');
  }


$id_pagar = $_POST['id_pagar'];
$comprovante  = $_FILES['arquivo'];

if($comprovante != null){
    $comprovante = file_get_contents($_FILES['arquivo']['tmp_name']);
    $comprovante = base64_encode($comprovante);
}

$sql = "select 
            c.valor,
            f.nome 
        from 
            contas_pagar as c
            inner join fornecedor as f ON
            c.id_fornecedor = f.id  
        where c.id = $id_pagar";

$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $valor = $row['valor'];
    $fornecedor = $row['nome'];
}




$valor_caixa = getValorCaixa();
$valor_caixa -= $valor;


$sql = "update contas_pagar set status = 1, comprovante = '$comprovante' where id = $id_pagar";
$res = mysqli_query($conn,$sql);

if($res){
    $texto_log = "CAIXA ANTIGO: ".getValorCaixa()." <br> CAIXA NOVO: $valor_caixa <br> CONTAS PAGAR VALOR: R$ $valor <br> FORNECEDOR: $fornecedor <br>  user: ".getUser($usuario_id);

    $sql = "insert into log (log) value ('$texto_log')";
    mysqli_query($conn,$sql);
    $sql = "update caixa set valor = $valor_caixa";
    mysqli_query($conn,$sql);

    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta Paga</div>";
    header("Location: ../index.php#contas_pagar");		

}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao pagar</div>";
    header("Location: ../index.php#contas_pagar");		

}

?>