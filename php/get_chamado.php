<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id_chamado = $_GET['id_chamado'];

$sql = "SELECT * FROM chamado WHERE id = $id_chamado";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	if($row['id_chamado']=="NULL"){
		$row['id_chamado'] = "";
	}
	array_push($data,array('id_chamado' => $row['id_chamado']));
	array_push($data,array('titulo' => $row['titulo']));
	array_push($data,array('data' => $row['data']));
	array_push($data,array('tempo' => $row['tempo']));
    array_push($data,array('semana' => $row['semana']));
    array_push($data,array('descricao' => $row['descricao']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>