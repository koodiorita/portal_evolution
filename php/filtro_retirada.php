<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}

$tipo = $_GET['tipo'];

if($tipo == 'Todos'){
    $tipo = "";
}else{
    $tipo = "where r.tipo = '$tipo'";
}

$sql = "select 
            u.nome,
            r.valor,
            r.descricao,
            r.data_cad
		from 
            retirada_dinheiro as r 
            left join user as u on
            r.id_user = u.id
         
            $tipo
		";
$res = mysqli_query($conn,$sql);
?>

<thead>
<tr>
    <th>Nome</th>
    <th>Valor</th>
    <th>Descrição</th>
    <th>Data</th>
</tr>
</thead>
<tfoot>
<tr>
    <th>Nome</th>
    <th>Valor</th>
    <th>Descrição</th>
    <th>Data</th>
</tr>
</tfoot>
<tbody>
    <?php
        
    while($row = mysqli_fetch_array($res)) { 
        
        ?>
        <tr>
            <td><?php echo $row['nome'];?></td>
            <td><?php echo "R$ ".number_format($row['valor'], 2, '.', '');?></td>
            <td><?php echo $row['descricao'];?></td>
            <td><?php echo date('d/m/Y',strtotime($row['data_cad']));?></td>
        </tr>
    <?php }?>	
</tbody>