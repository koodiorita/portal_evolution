<?php
include_once("../conn/conexao.php");

$id = $_GET['id'];
$sql = "SELECT * FROM contas_receber WHERE id_contrato = $id AND status = 0";
$res = mysqli_query($conn,$sql);

?>
<br>
<div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTableFinanceiro" width="100%" cellspacing="0" style="color:black;">
                  <thead>
                    <tr>
                      <th>Vencimento</th>
                      <th>Parcela</th>
                      <th>Valor</th>
                      <th>Pagamento / Tipo</th>
                    </tr>
                  </thead>
                  
                  <tbody>
						<?php
							$valor_total = 0;
						while($row = mysqli_fetch_array($res)) { 
							
							$valor = $row['valor_parcela'];

							$valor_total += $valor;
							?>
							<tr>
								<td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>
                                <td><?php echo $row['parcela'];?></td>
                                <td><?php echo "R$ ".number_format($valor, 2, '.', '');?></td>
                                <td><?php echo $row['tipo']." / ".$row['tipo_pagamento'];?></td>
							</tr>
						<?php }?>	
                  </tbody>
				  <tfoot>
                    <tr>
                      <th>Vencimento</th>
                      <th>Parcela</th>
                      <th><?php echo "R$ ".number_format($valor_total, 2, '.', '');?></th>
                      <th>Pagamento / Tipo</th>
                    </tr>
                  </tfoot>
                </table>
				
         </div>
</div>
<script>
	function buscar(id){
                    var data1 = $("#filtro-data1").val();
                    var data2 = $("#filtro-data2").val();

                    

                    $.get( "php/filtro_data.php?ini="+data1+"&fim="+data2+"&id_cliente="+id, function( data ) {
                        $("#dataTableFinanceiro").html(data);
                    });
                }
</script>