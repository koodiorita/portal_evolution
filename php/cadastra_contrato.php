<?php
error_reporting(0);
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getIdContrato(){
    global $conn;

    $sql = "select id from contrato order by id desc limit 1";
    $res = mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $id = $row['id'];
    }

    return $id;
}

function inserePagamentoImplementacao($id_pagamento,$id_servico,$desconto_imp,$id_contrato,$data_pagamento_imp){
    global $conn;
    
    $sql = "select * from pagamento where id = $id_pagamento";
    $res = mysqli_query($conn,$sql);
    
    while($row = mysqli_fetch_array($res)){
        $qtd_vezes = $row['qtd_vezes'];
        $tipo = $row['tipo'];
    }
    
    
    $sql = "select * from servico where id = $id_servico";
    $res = mysqli_query($conn,$sql);
    
    while($row = mysqli_fetch_array($res)){
        $valor = $row['valor'];
    }
    
    if($tipo == 'CARTAO DE CREDITO' ){
        $qtd_vezes = 1;
    }

    $valor_total = number_format(($valor - $desconto_imp) / $qtd_vezes, 2, '.', '');

    
    for($x = 0; $x < $qtd_vezes; $x++){

        $parcela = ($x + 1) ."/".$qtd_vezes;

        $sql = "insert into contas_receber(id_contrato,parcela,valor_parcela,vencimento,tipo,tipo_pagamento,status)
                values ($id_contrato,'$parcela',$valor_total,'$data_pagamento_imp','$tipo','IMPLANTACAO',0)";
        mysqli_query($conn,$sql); 
        
        $data_pagamento_imp = date('Y-m-d', strtotime('+30 days', strtotime($data_pagamento_imp)));
        
    }

}

function inserePagamentoMensal($id_contrato,$id_servico,$desconto,$primeiro_pagamento,$validade){
    global $conn;

    $sql = "select * from servico where id = $id_servico";
    $res = mysqli_query($conn,$sql);
    
    while($row = mysqli_fetch_array($res)){
        $valor = $row['valor_mensalidade'];
    }
    

    $valor = number_format(($valor - $desconto), 2, '.', '');

    for($x = 0; $x < $validade; $x++){

        $parcela = ($x + 1) ."/".$validade;

        $sql = "insert into contas_receber(id_contrato,parcela,valor_parcela,vencimento,tipo,tipo_pagamento,status)
                values ($id_contrato,'$parcela',$valor,'$primeiro_pagamento','BOLETO','MENSAL',0)";
        mysqli_query($conn,$sql);
        
        $primeiro_pagamento = date('Y-m-d', strtotime('+30 days', strtotime($primeiro_pagamento)));
        
    }

}


//Receber os dados do formulário
$id_orcamento       = $_POST['id_orcamento_con'] == NULL ? 0 : $_POST['id_orcamento_con'];
$id_cliente         = $_POST['cliente_con'];
$id_servico         = $_POST['servico_con'];
$id_pagamento       = $_POST['pagamento_imp_con'];
$data_pagamento_imp = $_POST['pagamento_con'];
$desconto_imp       = $_POST['desconto_pagamento_con'] == NULL ? 0 : $_POST['desconto_pagamento_con'];
$validade           = $_POST['validade_con'];
$primeiro_pagamento = $_POST['primeiro_pagamento_con'];
$desconto           = $_POST['desconto_con'] == NULL ? 0 : $_POST['desconto_con'];
$multa              = $_POST['multa_con'];
$descricao          = $_POST['descricao_con'];


$arquivo		= $_FILES['arquivo_con'];

$_UP['extensoes'] = array('pdf');

$extensao = strtolower(end(explode('.', $_FILES['arquivo_con']['name'])));
if (array_search($extensao, $_UP['extensoes']) === false) {
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Por favor, envie arquivos com as seguintes extensões: pdf</div>";
	header("Location: ../index.php#contrato"); 
}

//Validação dos campos
if(empty($_POST['cliente_con']) || empty($_POST['servico_con']) || empty($_POST['validade_con']) || empty($_POST['primeiro_pagamento_con']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#contrato"); 
}else{

    $arquivo = file_get_contents($_FILES['arquivo_con']['tmp_name']);
    $arquivo = base64_encode($arquivo);
	//Salvar no BD
    $result_data = 
    "INSERT 
        INTO 
            contrato(id_cliente, id_servico, id_pagamento, data_pagamento_imp, desconto_imp, descricao, 
            validade, desconto_mensal, id_multa, primeiro_vencimento, contrato,id_orcamento,id_status) 
        value($id_cliente,$id_servico,$id_pagamento,'$data_pagamento_imp',$desconto_imp,'$descricao',$validade,
        $desconto,$multa,'$primeiro_pagamento','$arquivo',$id_orcamento,6)";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){

        
        $id_contrato = getIdContrato();
        inserePagamentoImplementacao($id_pagamento,$id_servico,$desconto_imp,$id_contrato,$data_pagamento_imp);
        
        inserePagamentoMensal($id_contrato,$id_servico,$desconto,$primeiro_pagamento,$validade);

        
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Contrato cadastrado com sucesso</div>";
		header("Location: ../index.php#contrato");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar contrato</div>";
		header("Location: ../index.php#contrato");
	}
}


mysqli_close($conn);


?>