<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function check_conta($id){
    global $conn;

    $sql = "select * from contas_receber where id = $id and status = 0";
    $res = mysqli_query($sql);

    $row = mysqli_num_rows($res);


    return $row;
}

if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}


$id = $_GET['id_receber'];


if(check_conta($id) == 0){
    $sql = "update contas_receber set status = 2 where id = $id";
    $res = mysqli_query($conn,$sql);
    
    if($res){
        $texto_log = "CONTAS ANULADA ID RECEBIMENTO: R$ $id <br> user: $usuario_id";

        $sql = "insert into log (log) value ('$texto_log')";
        mysqli_query($conn,$sql);

        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta Anulada</div>";
        header("Location: index.php");
    }else{
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao anular</div>";
        header("Location: index.php");
    }
}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta ja foi dado baixa</div>";
    header("Location: index.php");
}


