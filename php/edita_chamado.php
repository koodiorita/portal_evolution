<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id = $_POST['id'];
$id_chamado = $_POST['id_chamado_edit'] ? $_POST['id_chamado_edit'] : 'NULL';
$titulo_chamado = $_POST['titulo_chamado_edit'];
$data_chamado = $_POST['data_chamado_edit'];
$tempo_chamado = $_POST['tempo_chamado_edit'];
$semana_chamado = $_POST['semana_chamado_edit'];
$descricao_chamado = $_POST['descricao_chamado_edit'];

//Validação dos campos
if(empty($_POST['titulo_chamado_edit']) || empty($_POST['data_chamado_edit']) || empty($_POST['tempo_chamado_edit']) || empty($_POST['semana_chamado_edit']) || empty($_POST['descricao_chamado_edit']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#chamado"); 
}else{
	//Salvar no BD
 	$sql = "UPDATE chamado SET id_chamado = '$id_chamado', titulo = '$titulo_chamado', data = '$data_chamado',
	 			tempo = '$tempo_chamado', semana = $semana_chamado, descricao = '$descricao_chamado' WHERE id = $id";
	$res = mysqli_query($conn, $sql);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($res){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Chamado editado com sucesso</div>";
		header("Location: ../index.php#chamado");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao editar chamado</div>";
		header("Location: ../index.php#chamado");
	}
	
}

mysqli_close($conn);
?>