<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$nome			= $_POST['nome_serv'];
$valor			= $_POST['valor_serv'];
$mensalidade	= $_POST['valor_mensalidade_serv'];
$contratacao	= $_POST['contratacao_serv'];
$descricao		= $_POST['descricao_serv'];


//Validação dos campos
if(empty($_POST['nome_serv']) || empty($_POST['valor_serv']) || empty($_POST['descricao_serv']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#servico"); 
}else{
	//Salvar no BD
 	$result_data = "INSERT INTO servico(nome,valor,descricao,valor_mensalidade,contratacao) value('$nome',$valor,'$descricao',$mensalidade,$contratacao)";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Serviço cadastrado com sucesso</div>";
		header("Location: ../index.php#servico");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar serviço</div>";
		header("Location: ../index.php#servico");
	}
	
}


mysqli_close($conn);


?>