<?php
include_once("../conn/conexao.php");

$id = $_GET['id'];
?>
<br>
<form action="php/cancela_contrato.php" method="post">
	<input type="hidden" class="form-control" name="id_cancelar" id="id_cancelar" value="<?= $id ?>" />
	<div class="form-row">
		<div class="col">
			<span>Multa: | </span>
		    <input name="collapseGroup" type="radio" data-toggle="collapse" data-target="#collapseOne"/> Sim
		    <input name="collapseGroup" type="radio" data-toggle="collapse" data-target="#collapseOne" checked/> Não
		    <div class="panel-group" id="accordion">
		      <div class="panel panel-default">
		        <div id="collapseOne" class="panel-collapse collapse">
		          <div class="panel-body">
		          	<span>Valor</span>
		            <input type="number" name="multa" id="multa" class="form-control" placeholder="Ex: 1000">
		            <span>Vencimento</span>
		            <input type="date" name="vencimentoMulta" id="vencimentoMulta" class="form-control">
		          </div>
		        </div>
		      </div>
		    </div>
		</div>
	</div><br>
	<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
	<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
</form>