<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['user_id'])) {
    $usuario_id = $_SESSION['user_id'];
} else {
    header('Location: login.php');
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$tipo = $_GET['tipo'];


$where = "";
if (!empty($data1)) {
    $where = "where c.vencimento between '$data1' AND '$data2'";
    if ($tipo != "") {
        $where .= " AND c.status = $tipo";
    }
} else {
    if ($tipo != "") {
        $where = "where c.status = $tipo";
    }
}

$id_cliente = isset($_GET['id_cliente']);

$cliente = "";
if (strlen($id_cliente) > 0) {
    $cliente = " and (cli.id = $id_cliente or cli2.id = $id_cliente)";
}
$where;
//2020-11-01 2020-11-30
$sql = "SELECT 
c.id,
c.parcela,
c.valor_parcela,
c.vencimento,
c.tipo,
c.tipo_pagamento,
cli.razao_social as responsavel_cli,
cli2.razao_social as responsavel_cli2,
sem.id_servico,
sem.valor_hora,
sem.qtd_hora,
c.status
FROM 
`contas_receber` as c
left join contrato as con ON
c.id_contrato = con.id
left join sem_contrato as sem ON
c.id_sem_contrato = sem.id
left join cliente as cli ON
con.id_cliente = cli.id 
left join cliente as cli2 ON
sem.id_cliente = cli2.id
$where $cliente";
$res = mysqli_query($conn, $sql);

$valor_pendente = 0;
$valor_sucess = 0;
$valor_cancel = 0;
?>
<table class="table table-bordered" id="tableFiltro">
    <thead>
        <tr>
            <th>Vencimento</th>
            <th>Parcela</th>
            <th>Valor</th>
            <th>Pagamento / Tipo</th>
            <th>Responsavel</th>
            <th width="10%">Receber</th>
            <th width="10%">Anular</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $valor_total = 0;
        while ($row = mysqli_fetch_array($res)) {
            $responsavel = $row['responsavel_cli'];

            if (strlen($responsavel) < 2) {
                $responsavel = $row['responsavel_cli2'];
            }

            $valor = $row['valor_parcela'];
            if ($row['id_servico'] == 99) {
                $valor = $row['qtd_hora'] * $row['valor_hora'];
            }

            if ($row['status'] == 0) {
                $valor_pendente += $valor;
            } elseif ($row['status'] == 1) {
                $valor_sucess += $valor;
            } else {
                $valor_cancel += $valor;
            }
        ?>
            <tr>
                <td><?php echo date('d/m/Y', strtotime($row['vencimento'])); ?></td>
                <td><?php echo $row['parcela']; ?></td>
                <td><?php echo "R$ " . number_format($valor, 2, '.', ''); ?></td>
                <td><?php echo $row['tipo'] . " / " . $row['tipo_pagamento']; ?></td>
                <td><?php echo $responsavel; ?></td>
                <?php if ($row['status'] == 0) { ?>
                    <td>
                        <center><button class="btn btn-primary btn-circle" onclick="receber(<?php echo $row['id']; ?>)"><i class="fas fa-download"></i></button></center>
                    </td>
                    <td>
                        <center><button class="btn btn-danger btn-circle" onclick="cancel(<?php echo $row['id']; ?>)"><i class="fas fa-window-close"></i></button></center>
                    </td>
                <?php } else if ($row['status'] == 1) { ?>
                    <td style="text-align: center;">Recebido</td>
                    <td style="text-align: center;">Recebido</td>
                <?php } else { ?>
                    <td style="text-align: center;">Cancelado</td>
                    <td style="text-align: center;">Cancelado</td>
                <?php } ?>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th>Vencimento</th>
            <th>Parcela</th>
            <th>Valor</th>
            <th>Pagamento / Tipo</th>
            <th>Responsavel</th>
            <th width="10%">Receber</th>
            <th width="10%">Anular</th>
        </tr>
    </tfoot>
</table>
<br>
<table border="1" class="informacoes">
    <tr>
        <th style="color:yellow;">Valor Pendente: <?= "R$ " . number_format($valor_pendente, 2, ".", ""); ?></th>
        <th style="color: #32CD32;">Valor Recebido: <?= "R$ " . number_format($valor_sucess, 2, ".", ""); ?></th>
        <th style="color:red;">Valor Cancelado: <?= "R$ " . number_format($valor_cancel, 2, ".", ""); ?></th>
    </tr>
</table>

<script>
    $(document).ready(function() {
        $('#tableFiltro').DataTable({});
    });

    function cancel(id_receber) {
        var resp = confirm("Deseja anular essa conta ?");

        if (resp == true) {
            $.get("php/anular_conta.php?id_receber=" + id_receber, function(data) {
                location.reload();
            });
        }
    }
</script>