<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id_chamado = $_POST['id_chamado'] ? $_POST['id_chamado'] : 'NULL';
$titulo_chamado = $_POST['titulo_chamado'];
$data_chamado = $_POST['data_chamado'];
$tempo_chamado = $_POST['tempo_chamado'];
$semana_chamado = $_POST['semana_chamado'];
$descricao_chamado = $_POST['descricao_chamado'];

//Validação dos campos
if(empty($_POST['titulo_chamado']) || empty($_POST['data_chamado']) || empty($_POST['tempo_chamado']) || empty($_POST['semana_chamado']) || empty($_POST['descricao_chamado']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#chamado"); 
}else{
	//Salvar no BD
 	$sql = "INSERT INTO chamado(id_chamado,titulo,data,tempo,semana,descricao) 
                VALUES('$id_chamado','$titulo_chamado','$data_chamado','$tempo_chamado',$semana_chamado,'$descricao_chamado')";
	$res = mysqli_query($conn, $sql);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Chamado cadastrado com sucesso</div>";
		header("Location: ../index.php#chamado");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar chamado</div>";
		header("Location: ../index.php#chamado");
	}
	
}

mysqli_close($conn);
?>