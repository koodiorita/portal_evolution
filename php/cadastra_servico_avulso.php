<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getIdContrato(){
    global $conn;

    $sql = "select id from sem_contrato order by id desc limit 1";
    $res = mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $id = $row['id'];
    }

    return $id;
}

// id_servico == 99 -> VALOR PAGO POR HORA
function inserePagamento($id_pagamento,$id_servico,$desconto,$id_contrato,$primeiro_pagamento,$qtd_hora,$valor_hora){
    global $conn;
    
    $sql = "select * from pagamento where id = $id_pagamento";
    $res = mysqli_query($conn,$sql);
    
    while($row = mysqli_fetch_array($res)){
        $qtd_vezes = $row['qtd_vezes'];
        $tipo = $row['tipo'];
    }
    
    
    $sql = "select * from servico where id = $id_servico";
    $res = mysqli_query($conn,$sql);
    
    while($row = mysqli_fetch_array($res)){
        $valor = $row['valor'];
    }
    
    if($tipo == 'CARTAO DE CREDITO'){
        $qtd_vezes = 1;
    }

    $valor_total = number_format(($valor - $desconto) / $qtd_vezes, 2, '.', '');

    if($id_servico == 99){
        $valor_total = ($qtd_hora * $valor_hora) / $qtd_vezes;
    }

    for($x = 0; $x < $qtd_vezes; $x++){

        $parcela = ($x + 1) ."/".$qtd_vezes;

        $sql = "insert into contas_receber(id_sem_contrato,parcela,valor_parcela,vencimento,tipo,tipo_pagamento,status)
                values ($id_contrato,'$parcela',$valor_total,'$primeiro_pagamento','$tipo','PAGAMENTO AVULSO',0)";
        mysqli_query($conn,$sql); 
        
        $primeiro_pagamento = date('Y-m-d', strtotime('+30 days', strtotime($primeiro_pagamento)));
        
    }

}




//Receber os dados do formulário
$id_cliente             = $_POST['cliente_avul'];
$id_servico             = $_POST['servico_avul'];
$id_pagamento           = $_POST['pagamento_avul'];
$primeiro_pagamento     = $_POST['primeiro_pagamento_avul'];
$desconto               = $_POST['desconto_avul'];
$descricao              = $_POST['descricao_avul'];
$qtd_hora               = $_POST['qtd_hora_avul'];
$valor_hora             = $_POST['valor_hora_avul'];

if($qtd_hora == NULL){
    $qtd_hora = 0;
    $valor_hora = 0;
}

//Validação dos campos
if(empty($_POST['cliente_avul']) || empty($_POST['servico_avul']) || empty($_POST['pagamento_avul']) || empty($_POST['primeiro_pagamento_avul']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#servico_avulso"); 
}else{

	//Salvar no BD
    echo $result_data = 
    "INSERT 
        INTO 
            sem_contrato(id_cliente, id_servico, id_pagamento, desconto, descricao,primeiro_pagamento,qtd_hora,valor_hora,id_status) 
        value($id_cliente,$id_servico,$id_pagamento,$desconto,'$descricao','$primeiro_pagamento',$qtd_hora,$valor_hora,6)";

	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){

        
        $id_contrato = getIdContrato();
        inserePagamento($id_pagamento,$id_servico,$desconto,$id_contrato,$primeiro_pagamento,$qtd_hora,$valor_hora);
        
        
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Serviço cadastrado com sucesso</div>";
		header("Location: ../index.php#servico_avulso");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar serviço</div>";
		header("Location: ../index.php#servico_avulso");
	}
	
}


mysqli_close($conn);


?>