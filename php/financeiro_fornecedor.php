<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}

$id = $_GET['id'];
$sql = "SELECT 
			c.id as 'conta_id',
			c.valor,
      c.id_fornecedor,
			c.vencimento,
			c.descricao,
			c.comprovante,
			c.status,
      c.data_cad,
      f.id as f_id
		FROM 
			`contas_pagar` as c
			inner join fornecedor as f ON
			c.id_fornecedor = f.id
			where 
			 
                f.id = $id

            order by c.vencimento asc
		";

$res = mysqli_query($conn,$sql);

?>

<div class="card-body">
              <div class="table-responsive">
                    <div class="form-row">
                        <div class="col-3">
                            <input type="date" id="filtro-data1" class="form-control" />
                        </div>
                        <span style="align-self: center;">até</span>
                        <div class="col-3">
                            <input type="date" id="filtro-data2" class="form-control" />
                        </div>
                        <div class="col-1">
                            <button  style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscar(<?php echo $id;?>)" >Buscar</button>
                        </div>
                    </div><br>
                <table class="table table-bordered" id="dataTableFinanceiro" width="100%" cellspacing="0" style="color:black;">
                  <thead>
                    <tr>
                      <th>Vencimento</th>
                      <th>Valor</th>
                      <th>Descrição</th>
                      <th width="10%">Pagar</th>
                    </tr>
                  </thead>
                  
                  <tbody>
						<?php
							$valor_total = 0;
  						while($row = mysqli_fetch_array($res)) {
              $valor_total += $row['valor']
							?>
							<tr>
								<td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>
                <td><?php echo "R$ ".number_format($row['valor'], 2, '.', '');?></td>
                <td><?php echo $row['descricao'];?></td>
								<?php if($row['status'] == 0) { ?>
								<td><center><button class="btn btn-primary btn-circle" onclick="pagar(<?php echo $row['conta_id'];?>)" ><i class="fas fa-download" ></i></button>
                </center></td>
								<?php }elseif ($row['status'] == 1){?>
								<td>Pago</td>
								<?php }else{?>
                <td>Cancelado</td>
                <?php } ?>
							</tr>
						<?php }?>	
                  </tbody>
				  <tfoot>
                    <tr>
                      <th>Vencimento</th>
                      <th><?php echo "R$ ".number_format($valor_total, 2, '.', '');?></th>
                      <th>Descrição</th>
                      <th width="10%">Pagar</th>
                    </tr>
                  </tfoot>
                </table>
				
              </div>
            </div>

            <div class="modal fade" id="PagarConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Pagar Conta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="php/pagar_conta.php" method="POST" enctype="multipart/form-data"  >
                              <input id="id_pagar" name="id_pagar" type="hidden"  value="<?= $row['conta_id'] ?>" />
                              <label>Insira o comprovante</label><br>
                              <input type="file" name="arquivo" class="form-control"><br>
                  <button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
                  <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                </form>
              </div>
                </div>
            </div>
            </div>

            <script>

                function pagar(id_pagar){
                  $('#id_pagar').val(id_pagar);
                  $('#PagarConta').modal('show');
                    
			    }    
                function buscar(id){
                    var data1 = $("#filtro-data1").val();
                    var data2 = $("#filtro-data2").val();

                    

                    $.get( "php/filtro_data.php?ini="+data1+"&fim="+data2+"&id_cliente="+id, function( data ) {
                        $("#dataTableFinanceiro").html(data);
                    });
                }
            </script>