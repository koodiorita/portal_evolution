<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getValorCaixa(){
    global $conn;

    $sql = "select valor from caixa";

    $res= mysqli_query($conn,$sql);

    while($row = mysqli_fetch_array($res)){
        $caixa = $row['valor'];
    }


    return $caixa;
}

function getUser($id_user){
    global $conn;
    $sql = "select * from user where id = $id_user";
    $res = mysqli_query($conn,$sql);
    while($row = mysqli_fetch_array($res)){
        $user = $row['usuario'];
    }

    return $user;
}


if(!empty($_SESSION['user_id'])){
    $usuario_id = $_SESSION['user_id'];
  }else{
      header('Location: login.php');
  }
// QUANDO FOR CARTÃO CREDITO, TEM UMA TAXA DA MAQUININHA 
$taxa_retirada_credito = 5.31;

// QUANDO FOR CARTÃO DEBITO, TEM UMA TAXA DA MAQUININHA 
$taxa_retirada_debito = 1.99;

$id_receber = $_GET['id_receber'];

$sql = "select * from contas_receber where id = $id_receber";

$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $valor = $row['valor_parcela'];
    $tipo  = $row['tipo'];
}

$taxa = 0;
if($tipo == 'CARTAO DE CREDITO'){
    $taxa = $taxa_retirada_credito;
}
if($tipo == 'CARTAO DE DEBITO'){
    $taxa = $taxa_retirada_debito;
}

$valor_taxa = ($valor * $taxa) / 100;

$valor_total = number_format($valor - $valor_taxa, 2, '.', '');



$valor_caixa = getValorCaixa();
$valor_caixa += $valor_total;

$texto_log = "CAIXA ANTIGO: ".getValorCaixa()." <br> CAIXA NOVO: $valor_caixa <br> CONTAS RECEBIDO VALOR: R$ $valor_total <br> TIPO: $tipo <br> TAXA: $taxa <br> user: ".getUser($usuario_id);

$sql = "insert into log (log) value ('$texto_log')";
mysqli_query($conn,$sql);

$sql = "update caixa set valor = $valor_caixa";
mysqli_query($conn,$sql);

$sql = "update contas_receber set status = 1 where id = $id_receber";
$res = mysqli_query($conn,$sql);

if($res){
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta Recebida</div>";
  //  header("Location: ../index.php#contas_receber");		
    echo "OK";
}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao receber</div>";
 //   header("Location: ../index.php#contas_receber");		
    echo "ERRO";
}

?>