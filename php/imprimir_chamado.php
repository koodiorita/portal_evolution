<?php
session_start();

require_once("../conn/conexao.php");

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];
$status = $_GET['status'];
// $tipo = $_GET['tipo'];


$where = "WHERE status = $status ";
if (!empty($data1)) {
    $where .= "AND data BETWEEN '$data1' AND '$data2'";
    // if ($tipo != "") {
    //     $where .= " AND c.status = $tipo";
    // }
} else {
    // if ($tipo != "") {
    //     $where = "where c.status = $tipo";
    // }
}

$sql = "SELECT
            *
        FROM chamado
        $where
        ";
$res = mysqli_query($conn, $sql);

$sql = "SELECT  
            SUM( TIME_TO_SEC(tempo) ) AS timeSum, semana
        FROM chamado 
        $where
        GROUP BY semana";
$res_soma_semana = mysqli_query($conn, $sql);

$sql = "SELECT  
            SUM( TIME_TO_SEC(tempo) )  AS timeSum, semana
        FROM chamado
        $where";
$res_total_soma = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_total_soma)) {
    $total_soma = $row[0];
}

//transforma em minutos
$total_soma /= 60;
$min_restante_total_soma = fmod($total_soma, 60);
$total_soma /= 60;
$total_soma = intval($total_soma);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="theme-color" content="#000">
    <title>Gestão | EvolutionSoft</title>

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <meta name="mobile-web-app-capable" content="yes">
    <!-- Custom styles for this template-->
    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <link href="../img/icon.png" rel="shortcut icon">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">
    <style>
        h4 {
            font-size: 1rem;
        }
    </style>
</head>

<body>
    <div class="header">
        <div class="form-row">
            <div class="col">
                <img style="margin-left: 10%;" src="../img/logoblack.png" />
            </div>
            <div class="col" style="align-self: center;">
                <label style="margin-left:20%;font-size:1.3rem;">EvolutionSoft, Itapetininga - SP</label><br>
                <label style="margin-left:20%;font-size:1.3rem;">R. Aristídes Lobo 323, Centro</label>
                <label style="margin-left:20%;font-size:1.3rem;"><b>CNPJ: </b> 37.308.765/0001-01</label>
            </div>
        </div>
    </div>

    <label class="divider"></label>
    <center>
        <h2 style="color:black">Chamados</h2>
        <h4><b>Tempo total: </b> <?= $total_soma ?>h e <?= $min_restante_total_soma ?>min</h4>
        <div class="form-row">
            <?php while ($row = mysqli_fetch_array($res_soma_semana)) {
                $row['timeSum'] /= 60;
                $min_restante_time_sum = fmod($row['timeSum'], 60);
                $row['timeSum'] /= 60;
                $row['timeSum'] = intval($row['timeSum']);
            ?>
                <div class="col">
                    <h4><b>Semana <?= $row['semana'] ?>:</b> <?= $row['timeSum'] ?>h e <?= $min_restante_time_sum ?>min</h4>
                </div>
            <?php } ?>
        </div>
    </center>
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="color:black;">
        <thead>
            <tr>
                <th>ID do Chamado</th>
                <th>Titulo</th>
                <th>Data</th>
                <th>Tempo</th>
                <th width="40%">Descrição</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID do Chamado</th>
                <th>Titulo</th>
                <th>Data</th>
                <th>Tempo</th>
                <th width="40%">Descrição</th>
            </tr>
        </tfoot>
        <tbody>
            <?php
            while ($row = mysqli_fetch_array($res)) {
                if ($row['id_chamado'] == "NULL") {
                    $id_chamado = "Sem informação";
                } else {
                    $id_chamado = $row['id_chamado'];
                }
            ?>
                <tr>
                    <td><?= $id_chamado ?></td>
                    <td><?= $row['titulo'] ?></td>
                    <td><?= date('d/m/Y', strtotime($row['data'])) ?></td>
                    <td><?= $row['tempo'] ?></td>
                    <td><?= $row['descricao']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
<!-- Bootstrap core JavaScript-->

<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="../vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts 
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>  -->
<!--<script src="//code.jquery.com/jquery-1.12.4.js"></script>-->
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/js/standalone/selectize.min.js"></script>

</html>

<script>
    window.print();
</script>