<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");


//Receber os dados do formulário
$cliente		= $_POST['cliente'];
$id_servico		= $_POST['servico'];
$responsavel	= $_POST['responsavel'];
$desconto		= $_POST['desconto'];


$arquivo		= $_FILES['arquivo_orc'];


$sql = "select * from cliente where razao_social = '$cliente'";
$res = mysqli_query($conn,$sql);

$id_cliente = 'NULL';
$nome_empresa = $cliente;
while($row = mysqli_fetch_array($res)){
    $nome_empresa = $row['razao_social'];
    $id_cliente   = $row['id'];
}

$_UP['extensoes'] = array('pdf');

$extensao = strtolower(end(explode('.', $_FILES['arquivo_orc']['name'])));
if (array_search($extensao, $_UP['extensoes']) === false) {
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Por favor, envie arquivos com as seguintes extensões: jpg, png ou jpeg</div>";
	header("Location: ../index.php#orcamento"); 
}

//Validação dos campos
if(empty($_POST['cliente']) || empty($_POST['servico']) || empty($_POST['responsavel'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#orcamento"); 
}else{

    $arquivo = file_get_contents($_FILES['arquivo_orc']['tmp_name']);
    $arquivo = base64_encode($arquivo);
	//Salvar no BD
	$result_data = "INSERT INTO orcamento(id_cliente,id_servico,nome_empresa,responsavel,orcamento,desconto) 
    value($id_cliente,$id_servico,'$nome_empresa','$responsavel','$arquivo',$desconto)";

	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Orçamento cadastrado com sucesso</div>";
		header("Location: ../index.php#orcamento");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar orçamento</div>";
		header("Location: ../index.php#orcamento");
	}
	
}


mysqli_close($conn);


?>