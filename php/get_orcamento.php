<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id_orcamento = $_GET['id_orcamento'];

$sql = "select o.id_cliente,o.id_servico,o.desconto,c.razao_social,s.nome from orcamento as o inner join cliente as c on o.id_cliente = c.id inner join servico as s on o.id_servico = s.id where o.id = $id_orcamento";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data, array('id_cliente' =>$row['id_cliente']));
	array_push($data, array('nome_cliente' =>$row['razao_social']));
	array_push($data, array('id_servico' =>$row['id_servico']));
	array_push($data, array('nome_servico' =>$row['nome']));
	array_push($data, array('desconto' =>$row['desconto']));
	array_push($data, array('id_orcamento' =>$id_orcamento));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>