
         <div class="modal fade" id="AddServ" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Serviço</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_servico.php" method="POST"  >
                        <div class="form-row">
                            <div class="col">
                                <input name="nome_serv" type="text" placeholder="Nome da Serviço" class="form-control" required /><br>
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col">
                                <input name="valor_serv" type="number" step="0.1" placeholder="Valor" class="form-control" required /><br>
                            </div>
							<div class="col">
                                <input name="valor_mensalidade_serv" type="number" step="0.1" placeholder="Valor Mensalidade" class="form-control" required /><br>
                            </div>
                        </div>

						<div class="form-row">
							<div class="col">Esse serviço é obrigatório um contrato ?</div>
						</div>
						<div class="form-row">
                            <div class="col">
                                <select name="contratacao_serv" class="form-control">
									<option value="1">Sim</option>
									<option value="0">Não</option>
								</select>
                            </div>
                        </div><br>
                        <textarea class="form-control" name="descricao_serv" placeholder="Descrição" ></textarea></br>
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>