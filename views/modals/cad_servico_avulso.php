<?php
    $sql = "select * from cliente where status = 1";
    $resCliente = mysqli_query($conn,$sql);

    $sql = "select * from servico where contratacao = 0";
    $resServico = mysqli_query($conn,$sql);

    $sql = "select * from pagamento";
    $resPagamento = mysqli_query($conn,$sql);

?>
    <div class="modal fade" id="AddServicoAvulso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Novo Serviço</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_servico_avulso.php" method="POST" >
                        <!--<input type="hidden" id="id_orcamento_con" name="id_orcamento_con" />-->
                        <div class="form-row">
                            <div class="col" >
                            Cliente
                            </div>
                            <div class="col" >
                            Serviço
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <select class="form-control" name="cliente_avul" >
                                    <option id="id_cliente_avul" value="">Selecione um Cliente</option>
                                    <?php 
                                        while($row = mysqli_fetch_array($resCliente)){ ?>
                                            <option value="<?php echo $row['id'];?>"><?php echo$row['razao_social'];?></option>
                                        <?php }?>
                                </select></br>
                            </div>
                            <div class="col">
                                <select class="form-control" name="servico_avul" id="servico_avul">
                                    <option id="id_servico_avul" value="">Selecione um Serviço</option>
                                    <option value="99">Serviço Por Hora</option>
                                    <?php 
                                        while($row = mysqli_fetch_array($resServico)){ ?>
                                            <option  value="<?php echo $row['id'];?>"><?php echo$row['nome'];?></option>
                                        <?php }?>
                                </select></br>
                            </div>
                        </div>

                        <div class="form-row" id="porhora" style="display: none">
                            <div class="col">
                                <input name="qtd_hora_avul" type="number" placeholder="Quantidade de Hora" class="form-control">
                            </div>
                            <div class="col">
                                <input name="valor_hora_avul" type="number" placeholder="Valor/Hora" step="0.01" class="form-control">
                            </div>
                        </div>
                        <hr><br>
                        <h3>Pagamento</h3>

                        <div class="form-row">
                            <div class="col-6">
                                Forma de Pagamento
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-6">
                                <select name="pagamento_avul" id="pagamento_avul" class="form-control" data-teste="teste">
                                    <option value="">Selecione um Pagamento</option>
                                <?php while($row = mysqli_fetch_array($resPagamento)){?>
                                    <option data-juros="<?php echo $row['juros'];?>" data-vezes="<?php echo $row['qtd_vezes'];?>" value="<?php echo $row['id'];?>"><?php echo $row['nome'];?></option>
                                <?php }?>    
                                </select>    
                            </div>
                           
                        </div><br>

                        <div class="form-row">
                            <div class="col">Data Pagamento</div>
                            <div class="col">Desconto</div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                 <input id="primeiro_pagamento_avul" name="primeiro_pagamento_avul" type="date" class="form-control" readonly />
                            </div>
                            <div class="col">
                                <input class="form-control" id ="desconto_avul" name="desconto_avul" type="number" step="0.1" placeholder="Ex: 100" readonly/>
                            </div>
                        </div>
                    
                        <br>
                        
                        <div class="form-row">
                            <div class="col">Descrição</div>
                        </div>
                        <textarea class="form-control" name="descricao_avul" placeholder=". . ." ></textarea></br>
                     
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>

          <script>
                
                $( "#pagamento_avul" ).change(function() {
                    //alert($(this).val());
                    var juros = $(this).find(':selected').attr('data-juros');
                    var vezes = $(this).find(':selected').attr('data-vezes');
                    
                    if($(this).val() != 1){
                        
                        $("#juros").val(juros);
                        $("#juros").css('display','block');
                        $("#vezes").val(vezes);
                        $("#vezes").css('display','block');
                        $("#primeiro_pagamento_avul").attr("readonly", false); 
                        $("#desconto_avul").attr("readonly", false); 
                    }else{
                        $("#primeiro_pagamento_avul").attr("readonly", true); 
                        $("#desconto_avul").attr("readonly", true); 
                        $("#juros").val(juros);
                        $("#vezes").val(vezes);
                    }

                    
                });

                $( "#servico_avul" ).change(function() {
                    //alert($(this).val());
                    
                    if($(this).val() == 99){
                        $("#porhora").css('display','flex');
                    }else{
                        $("#porhora").css('display','none');
                    }

                    
                });

          </script>