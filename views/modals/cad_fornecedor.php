	<!-- AddProd -->
    <div class="modal fade" id="AddFor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Fornecedor</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_fornecedor.php" method="POST"  >
                        <div class="form-row">
                            <div class="col">
                                <input name="nome_for" type="text" placeholder="Nome da Fornecedor" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input name="cnpj_for" id="cnpj_for" type="text" placeholder="CPF / CNPJ" class="form-control"  /><br>
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep_for" name="cep_for" type="text" placeholder="CEP" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCepFor" type="button"  class="btn btn-primary" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco_for" name="endereco_for" type="text" placeholder="Endereço completo" class="form-control" required/><br>
                            </div>
                            <div class="col-4">
                                <input id="numero_for" name="numero_for" type="text" placeholder="Numero" class="form-control" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro_for" name="bairro_for" type="text" placeholder="Bairro" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade_for" name="cidade_for" type="text" placeholder="Cidade" class="form-control" required /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone_for" type="text" placeholder="Telefone" class="form-control" required />
                            </div>
                            <div class="col">
                                <input name="telefone2_for" type="text" placeholder="Telefone 2 (Opcional)" class="form-control" /></br>
                            </div>
                            
                        </div>
                                
						
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
        
        <script>
        $(document).ready(function () {
            $("#buscaCepFor").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_for").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_for").val(dados.logradouro);
                            $("#endereco_for").css("background","#eee");
                            $("#bairro_for").val(dados.bairro);
                            $("#bairro_for").css("background","#eee");
                            $("#cidade_for").val(dados.localidade);
                            $("#cidade_for").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });
        </script>