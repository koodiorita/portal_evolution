<div class="modal fade" id="AddCham" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Chamado</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/cadastra_chamado.php" method="POST">
					<div class="form-row">
						<div class="col">
							<input name="id_chamado" type="text" placeholder="ID do chamado" class="form-control" />
						</div>
						<div class="col">
							<input type="text" name="titulo_chamado" placeholder="Titulo do chamado" class="form-control" required /><br>
						</div>
					</div>
					

					<div class="form-row">
						<div class="col">Data:</div>
						<div class="col">Tempo:</div>
					</div>

					<div class="form-row">
						<div class="col">
							<input name="data_chamado" id="data_chamado" type="date" class="form-control" required /><br>
						</div>
						<div class="col">
							<input name="tempo_chamado" id="tempo_chamado" type="time" class="form-control" required /><br>
						</div>
					</div>

					<div class="form-row">
						<div class="col">Semana do chamado:</div>
					</div>
					<div class="form-row">
						<div class="col">
							<select name="semana_chamado" class="form-control" required>
								<option value="1">Semana 1</option>
								<option value="2">Semana 2</option>
								<option value="3">Semana 3</option>
								<option value="4">Semana 4</option>
							</select>
						</div>
					</div><br>
					<textarea class="form-control" name="descricao_chamado" placeholder="Descrição" required></textarea></br>
					<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>