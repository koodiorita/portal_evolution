	 <?php
      $sql = "select * from servico";
      $res = mysqli_query($conn,$sql);
      ?>
     
     
         <!-- AddOrcamento -->
         <div class="modal fade" id="AddOrc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Novo Orçamento</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
					</div>
					<div class="modal-body">
						<form action="php/cadastra_orcamento.php" method="POST" enctype="multipart/form-data" >
                            <div class="form-row">
                                <div class="col">
                                    <input id="cliente" name="cliente" type="text" class="form-control" placeholder="Nome do cliente"/></br>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <select  name="servico" class="form-control">
                                        <option >Selecione um Serviço</option>
                                        <?php 
                                        while($row = mysqli_fetch_array($res)){ ?> 
                                            <option value="<?php echo $row['id'];?>"><?php echo $row['nome'];?></option>
                                        <?php }?>
                                    </select></br>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <input id="responsavel" name="responsavel" type="text" class="form-control" placeholder="Responsavel"/></br>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <input id="desconto" name="desconto" type="number" step="0.10" class="form-control" placeholder="Desconto" /></br>
                                </div>
                            </div>

							<input type="file" name="arquivo_orc" class="form-control"  /><br>
							<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
							<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
						</form>
					</div>
				  </div>
			</div>
		  </div>

          <script>
            $(function() {
                $.get( "php/get_clientes.php", function( data ) {
                    var clientes = JSON.parse(data);
                
                $("#cliente" ).autocomplete({
                    source: clientes
                });
                });
            });

          </script>