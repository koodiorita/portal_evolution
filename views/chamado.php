<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['user_id'])) {
	$usuario_id = $_SESSION['user_id'];
} else {
	header('Location: login.php');
}

$sql = "SELECT 
			* 
		FROM 
			chamado
		WHERE status = 0
		";
$res = mysqli_query($conn, $sql);
$count_chamado = mysqli_num_rows($res);

$sql = "SELECT  
            SUM( TIME_TO_SEC(tempo) ) AS timeSum, semana
        FROM chamado 
		WHERE status = 0
		GROUP BY semana";
$res_soma_semana = mysqli_query($conn, $sql);

$sql = "SELECT  
            SUM( TIME_TO_SEC(tempo) )  AS timeSum, semana
        FROM chamado
		WHERE status = 0";
$res_total_soma = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($res_total_soma)) {
	$total_soma = $row[0];
}

//transforma em minutos
$total_soma /= 60;
$min_restante_total_soma = fmod($total_soma, 60);
$total_soma /= 60;
$total_soma = intval($total_soma);
//
?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}
</style>
<div class="container-fluid">
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="form-row">
				<div class="col">
					<h4 class="m-0 font-weight-bold text-primary">Chamados</h4>
				</div>
				<div class="col-2"><input type="date" id="filtro-data1" class="form-control" /></div>
				<span style="align-self: center;">até</span>
				<div class="col-2"><input type="date" id="filtro-data2" class="form-control" /></div>
				<input type="hidden" value="<?= $count_chamado ?>" id="count_chamado" name="count_chamado">
				<!-- <div class="col-2">
					<select class="form-control" name="tipoPesquisa" id="tipoPesquisa">
						<option value="">Selecione o tipo</option>
						<option value="0">Em aberto</option>
						<option value="1">Recebido</option>
						<option value="2">Cancelado</option>
					</select>
				</div> -->
				<div class="col-2"><button style="float: right;margin-left: 10px" class=" btn btn-primary" onclick="buscar()">Buscar</button></div>
				<button style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddCham">Adicionar</button>
				<button style="float: right;margin-left: 10px" class=" btn btn-info" onclick="imprimir_chamado()">
					<i class="fas fa-print"></i> Imprimir
				</button>
				<button style="float: right;margin-left: 10px" class=" btn btn-info" onclick="finalizar()">Finalizar</button>
			</div>
		</div>
		<div class="card-body" id="body-table">
			<div class="table-responsive">
				<table class="table table-bordered" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th colspan="4" class="text-center"><b>Tempo total: </b> <?= $total_soma ?>h e <?= $min_restante_total_soma ?>min</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<?php while ($row = mysqli_fetch_array($res_soma_semana)) {
								$row['timeSum'] /= 60;
								$min_restante_time_sum = fmod($row['timeSum'], 60);
								$row['timeSum'] /= 60;
								$row['timeSum'] = intval($row['timeSum']);
							?>
								<th><b>Semana <?= $row['semana'] ?>:</b> <?= $row['timeSum'] ?>h e <?= $min_restante_time_sum ?>min</th>
							<?php } ?>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTableChamado" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th width="5%">Check</th>
							<th>ID do Chamado</th>
							<th>Titulo</th>
							<th>Data</th>
							<th>Tempo</th>
							<th>Semana</th>
							<th width="40%">Descrição</th>
							<th width="10%">Editar</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th width="5%">Check</th>
							<th>ID do Chamado</th>
							<th>Titulo</th>
							<th>Data</th>
							<th>Tempo</th>
							<th>Semana</th>
							<th width="40%">Descrição</th>
							<th width="10%">Editar</th>
						</tr>
					</tfoot>
					<tbody id="tbody-chamado">
						<?php
						$i = 1;
						while ($row = mysqli_fetch_array($res)) {
							if ($row['id_chamado'] == "NULL") {
								$id_chamado = "Sem informação";
							} else {
								$id_chamado = $row['id_chamado'];
							}
						?>
							<tr>
								<td>
									<center>
										<input type="checkbox" class="form-control" style="width:25px;" id="chamado_<?= $i ?>" value="<?= $row['id'] ?>">
									</center>
								</td>
								<td><?= $id_chamado ?></td>
								<td><?= $row['titulo'] ?></td>
								<td><?= date('d/m/Y', strtotime($row['data'])) ?></td>
								<td><?= $row['tempo'] ?></td>
								<td><?= "Semana" . $row['semana'] ?></td>
								<td><?= $row['descricao']; ?></td>
								<td>
									<center>
										<button class="btn btn-warning btn-circle" onclick="edit_chamado(<?= $row['id'] ?>)">
											<i class="fas fa-edit"></i>
										</button>
									</center>
								</td>
							</tr>
						<?php
							$i++;
						} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="EditCham" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Chamado</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="php/edita_chamado.php" method="POST">
					<input type="hidden" id="id" name="id">
					<div class="form-row">
						<div class="col">
							<input name="id_chamado_edit" id="id_chamado_edit" type="text" placeholder="ID do chamado" class="form-control" />
						</div>
						<div class="col">
							<input type="text" name="titulo_chamado_edit" id="titulo_chamado_edit" placeholder="Titulo do chamado" class="form-control" required /><br>
						</div>
					</div>


					<div class="form-row">
						<div class="col">Data:</div>
						<div class="col">Tempo:</div>
					</div>

					<div class="form-row">
						<div class="col">
							<input name="data_chamado_edit" id="data_chamado_edit" type="date" class="form-control" required /><br>
						</div>
						<div class="col">
							<input name="tempo_chamado_edit" id="tempo_chamado_edit" type="time" class="form-control" required /><br>
						</div>
					</div>

					<div class="form-row">
						<div class="col">Semana do chamado:</div>
					</div>
					<div class="form-row">
						<div class="col">
							<select name="semana_chamado_edit" id="semana_chamado_edit" class="form-control" required>
								<option value="1">Semana 1</option>
								<option value="2">Semana 2</option>
								<option value="3">Semana 3</option>
								<option value="4">Semana 4</option>
							</select>
						</div>
					</div><br>
					<textarea class="form-control" name="descricao_chamado_edit" id="descricao_chamado_edit" placeholder="Descrição" required></textarea></br>
					<button class="btn btn-success" type="submit" style="float: right">Editar</button>
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#dataTableChamado').DataTable({});
	});

	function edit_chamado(id) {
		$.get("php/get_chamado.php?id_chamado=" + id, function(data) {
			var json = JSON.parse(data);
			$("#id").val(id);
			$("#id_chamado_edit").val(json[0].id_chamado);
			$("#titulo_chamado_edit").val(json[1].titulo);
			$("#data_chamado_edit").val(json[2].data);
			$("#tempo_chamado_edit").val(json[3].tempo);
			$("#semana_chamado_edit").val(json[4].semana);
			$("#descricao_chamado_edit").val(json[5].descricao);

			$('#EditCham').modal('show');

		});
	}

	function imprimir_chamado() {
		// var filtro_responsavel = $("#filtro_responsavel").val();
		// var filtro_variedade = $("#filtro_variedade").val();
		var data1 = $("#filtro-data1").val();
		var data2 = $("#filtro-data2").val();

		// window.open("php/imprimir_rel_semeacao.php?filtro_responsavel=" + filtro_responsavel + "&filtro_variedade="+ filtro_variedade + "&filtro_data_ini=" + filtro_data_ini + "&filtro_data_fim=" + filtro_data_fim);
		window.open("php/imprimir_chamado.php?ini=" + data1 + "&fim=" + data2 + "&status=0");
	}

	function buscar() {
		var data1 = $("#filtro-data1").val();
		var data2 = $("#filtro-data2").val();
		// var tipo = $("#tipoPesquisa").val();

		if (data1.length > 0) {
			// + "&tipo=" + tipo
			$.get("php/filtro_chamado.php?ini=" + data1 + "&fim=" + data2 + "&status=0", function(data) {
				$("#body-table").html(data);
			});
		} else {
			alert('Preencha os campos de data.');
		}
	}

	var string_ids = "";

	function finalizar() {
		var contagem = $('#count_chamado').val();

		for (var i = 1; i <= contagem; i++) {
			if ($('#chamado_' + i).is(':checked')) {
				// 1;3;
				string_ids += $('#chamado_' + i).val() + ";";
			}
		}

		$('#ids_chamados').val(string_ids);

		console.log(string_ids);
		console.log(contagem);
		$.get("php/finalizar_chamados.php?ids_chamados=" + string_ids, function(data) {
			if (data == "OK") {
				location.reload();
			}
		});
	}
</script>