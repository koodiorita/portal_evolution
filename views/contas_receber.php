<?php
session_start();

require_once("../conn/conexao.php");


if (!empty($_SESSION['user_id'])) {
	$usuario_id = $_SESSION['user_id'];
} else {
	header('Location: login.php');
}


$sql = "SELECT 
			c.id,
			c.parcela,
			c.valor_parcela,
			c.vencimento,
			c.tipo,
			c.tipo_pagamento,
			cli.razao_social as responsavel_cli,
			cli2.razao_social as responsavel_cli2,
			sem.id_servico,
			sem.valor_hora,
			sem.qtd_hora,
			c.status
		FROM 
			`contas_receber` as c
			left join contrato as con ON
			c.id_contrato = con.id
			left join sem_contrato as sem ON
			c.id_sem_contrato = sem.id
			left join cliente as cli ON
			con.id_cliente = cli.id 
			left join cliente as cli2 ON
			sem.id_cliente = cli2.id 
			where 
			month(c.vencimento) = month(now()) and year(c.vencimento) = year(now())
		";

$res = mysqli_query($conn, $sql);

$sql = "SELECT * FROM pagamento";
$resPagamento = mysqli_query($conn, $sql);

$valor_pendente = 0;
$valor_sucess = 0;
$valor_cancel = 0;
?>
<style>
	.onoff input.toggle {
		display: none;
	}

	.onoff input.toggle+label {
		display: inline-block;
		position: relative;
		box-shadow: inset 0 0 0px 1px #d5d5d5;
		height: 20px;
		width: 40px;
		border-radius: 30px;
	}

	.onoff input.toggle+label:before {
		content: "";
		display: block;
		height: 20px;
		width: 40px;
		border-radius: 30px;
		background: rgba(19, 191, 17, 0);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle+label:after {
		content: "";
		position: absolute;
		height: 20px;
		width: 20px;
		top: 0;
		left: 0px;
		border-radius: 30px;
		background: #fff;
		box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
		transition: 0.1s ease-in-out;
	}

	.onoff input.toggle:checked+label:before {
		width: 40px;
		background: #13bf11;
	}

	.onoff input.toggle:checked+label:after {
		left: 20px;
		box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
	}

	.xx {
		float: right;
		background: #ccc;
		border-radius: 200px;
		width: 14px;
		height: 13px;
		color: white;
		text-align: center;
		font-size: 10px;
	}

	.xx:hover {
		background: #777;
		cursor: pointer
	}

	.dataTables_wrapper .dataTables_filter input {
		border-radius: 10px;
		border: 1px solid #ccc;
		outline-style: none;
	}

	.informacoes {
		border: solid 1px;
		border-color: #e3e6f0;
		text-align: center;
		vertical-align: middle;
		font-size: 1rem;
		font-weight: bold;
		width: 100%;
	}
</style>
<div class="container-fluid">



	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="form-row">
				<div class="col">
					<h4 class="m-0 font-weight-bold text-primary">Contas à Receber</h4>
				</div>

				<div class="col-2"><input type="date" id="filtro-data1" class="form-control" /></div>
				<span style="align-self: center;">até</span>
				<div class="col-2"><input type="date" id="filtro-data2" class="form-control" /></div>
				<div class="col-2">
					<select class="form-control" name="tipoPesquisa" id="tipoPesquisa">
						<option value="">Selecione o tipo</option>
						<option value="0">Em aberto</option>
						<option value="1">Recebido</option>
						<option value="2">Cancelado</option>
					</select>
				</div>
				<div class="col-2"><button style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscar()">Buscar</button></div>
			</div>
		</div>

		<div class="card-body">
			<div class="table-responsive">
				<div class="conteudoTable" id="conteudoTable">
					<table class="table table-bordered" id="contasReceberTable" width="100%" cellspacing="0">

						<thead>
							<tr>
								<th>Vencimento</th>
								<th>Parcela</th>
								<th>Valor</th>
								<th>Pagamento</th>
								<th>Responsavel</th>
								<th width="10%">Receber</th>
								<th width="10%">Anular</th>
							</tr>
						</thead>

						<tbody>
							<?php
							$valor_total = 0;
							while ($row = mysqli_fetch_array($res)) {
								$responsavel = $row['responsavel_cli'];

								if (strlen($responsavel) < 2) {
									$responsavel = $row['responsavel_cli2'];
								}

								$valor = $row['valor_parcela'];
								if ($row['id_servico'] == 99) {
									$valor = $row['qtd_hora'] * $row['valor_hora'];
								}
								if ($row['status'] == 0) {
									$valor_pendente += $valor;
								} elseif ($row['status'] == 1) {
									$valor_sucess += $valor;
								} else {
									$valor_cancel += $valor;
								}
							?>
								<tr>
									<td><?php echo date('d/m/Y', strtotime($row['vencimento'])); ?></td>
									<td><?php echo $row['parcela']; ?></td>
									<td><?php echo "R$ " . number_format($valor, 2, '.', ''); ?></td>
									<td><?php echo $row['tipo']; ?></td>
									<td><?php echo $responsavel; ?></td>

									<?php if ($row['status'] == 0) { ?>
										<td>
											<center>
												<button class="btn btn-primary btn-circle" onclick="receber(<?php echo $row['id']; ?>)"><i class="fas fa-download"></i></button>
											</center>
										</td>
										<td>
											<center>
												<button class="btn btn-danger btn-circle" onclick="cancel(<?php echo $row['id']; ?>)"><i class="fas fa-window-close"></i></button>
											</center>
										</td>

									<?php } elseif ($row['status'] == 1) { ?>
										<td style="text-align: center;">Recebido</td>
										<td style="text-align: center;">Recebido</td>

									<?php } else { ?>
										<td style="text-align: center;">Cancelado</td>
										<td style="text-align: center;">Cancelado</td>

									<?php } ?>
								</tr>
							<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Vencimento</th>
								<th>Parcela</th>
								<th>Valor</th>
								<th>Pagamento</th>
								<th>Responsavel</th>
								<th width="10%">Receber</th>
								<th width="10%">Anular</th>
							</tr>
						</tfoot>
					</table>
					<br>
					<table border="1" class="informacoes">
						<tr>
							<th style="color:yellow;">Valor Pendente: <?= "R$ " . number_format($valor_pendente, 2, ".", ""); ?></th>
							<th style="color: #32CD32;">Valor Recebido: <?= "R$ " . number_format($valor_sucess, 2, ".", ""); ?></th>
							<th style="color:red;">Valor Cancelado: <?= "R$ " . number_format($valor_cancel, 2, ".", ""); ?></th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function() {
		$('#contasReceberTable').DataTable({});
	});

	function receber(id_receber) {

		var resp = confirm("Deseja receber essa conta ?");

		if (resp == true) {
			$.get("php/receber_conta.php?id_receber=" + id_receber, function(data) {
				location.reload();
			});
		}

	}

	function cancel(id_receber) {
		var resp = confirm("Deseja anular essa conta ?");

		if (resp == true) {
			$.get("php/anular_conta.php?id_receber=" + id_receber, function(data) {
				location.reload();
			});
		}
	}

	function buscar() {
		var data1 = $("#filtro-data1").val();
		var data2 = $("#filtro-data2").val();
		var tipo = $("#tipoPesquisa").val();

		if (data1.length > 0 || tipo != "") {
			$.get("php/filtro_data.php?ini=" + data1 + "&fim=" + data2 + "&tipo=" + tipo, function(data) {
				$("#conteudoTable").html(data);
			});
		} else {
			alert('Preencha pelo menos um dos campos.');
		}
	}
</script>