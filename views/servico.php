<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}


$sql = "select 
			* 
		from 
			servico
		";
$res = mysqli_query($conn,$sql);



	
?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Serviços
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddServ" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="15%">Nome</th>
                      <th>Valor Imp</th>
                      <th>Valor Mensal</th>
                      <th width="50%">Descrição</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th width="15%">Nome</th>
                      <th>Valor Imp</th>
                      <th>Valor Mensal</th>
                      <th width="50%">Descrição</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </tfoot>
                  <tbody>
						<?php
							
						while($row = mysqli_fetch_array($res)) { 
							$valor 			= $row['valor'];
							$valor_mensal 	= $row['valor_mensalidade'];

							if($valor == 0){
								$valor = '- - - -';
							}else{
								$valor = "R$ ".number_format($row['valor'], 2, '.', '');
							}
							if($valor_mensal == 0){
								$valor_mensal = '- - - -';
							}else{
								$valor_mensal = "R$ ".number_format($row['valor_mensalidade'], 2, '.', '');
							}
							?>
							<tr>
								<td><?php echo $row['nome'];?></td>
								<td><?php echo $valor;?></td>
								<td><?php echo $valor_mensal;?></td>
                                <td><?php echo $row['descricao'];?></td>
								<td><center><button class="btn btn-warning btn-circle" onclick="edit_servico(<?php echo $row['id'];?>)" ><i class="fas fa-edit" ></i></button></center></td>
							</tr>
						<?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
				
		<div class="modal fade" id="EditServ" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Serviço</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/edita_servico.php" method="POST"  >
					<input type="hidden" id="id_serv_edit" name="id_serv_edit">
                        <div class="form-row">
                            <div class="col">
                                <input name="nome_serv_edit" id="nome_serv_edit" type="text" placeholder="Nome da Serviço" class="form-control" required /><br>
                            </div>
                        </div>
                            
                        <div class="form-row">
                            <div class="col">
                                <input name="valor_serv_edit" id="valor_serv_edit" type="number" step="0.1" placeholder="Valor" class="form-control" required /><br>
                            </div>
							<div class="col">
                                <input name="valor_mensalidade_serv_edit" id="valor_mensalidade_serv_edit" type="number" step="0.1" placeholder="Valor Mensalidade" class="form-control" required /><br>
                            </div>
                        </div>

						<div class="form-row">
							<div class="col">Esse serviço é obrigatório um contrato ?</div>
						</div>
						<div class="form-row">
                            <div class="col">
                                <select name="contratacao_serv_edit" class="form-control" id="contratacao_serv_edit">
									<option value="1">Sim</option>
									<option value="0">Não</option>
								</select>
                            </div>
                        </div><br>
                        <textarea class="form-control" name="descricao_serv_edit" placeholder="Descrição" id="descricao_serv_edit" ></textarea></br>
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                });
            });

			function edit_servico(id){
			$.get( "php/get_servico.php?id_servico="+id, function( data ) {
					 var json = JSON.parse(data);
					$( "#id_serv_edit" ).val( id );
					$( "#nome_serv_edit" ).val( json[0].nome );
					$( "#valor_serv_edit" ).val( json[1].valor );
					$( "#valor_mensalidade_serv_edit" ).val( json[2].valor_mensalidade );
					$( "#descricao_serv_edit" ).val( json[3].descricao );
					$( "#contratacao_serv_edit" ).val( json[4].contratacao );
					
					$('#EditServ').modal('show');

				});
				
		}
		</script>