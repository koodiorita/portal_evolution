<?php 
session_start();

require_once("../conn/conexao.php");

function checkContrato($id_orcamento){
	global $conn;

	$sql = "select id from contrato where id_orcamento = $id_orcamento";
	$res = mysqli_query($conn,$sql);

	$row = mysqli_num_rows($res);

	$return = false;
	if($row > 0){
		$return =true;
	}


	return $return;
}

if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}


$sql = "select 
            o.id,
            c.razao_social as nome_cliente,
            s.nome as nome_servico,
            s.valor as valor_imp,
			s.valor_mensalidade as valor_mensal,
			o.desconto,
            st.status as status_nome,
            o.responsavel,
            o.nome_empresa,
			st.color,
			o.id_status,
			s.contratacao,
			o.id_cliente
        from 
            orcamento as o 
            LEFT join cliente as c on
            o.id_cliente = c.id
            inner join servico as s on
            o.id_servico = s.id 
            inner join status as st on
            o.id_status = st.id
		";
$res = mysqli_query($conn,$sql);



	
?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Orçamentos
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddOrc" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Cliente</th>
                      <th>Serviço</th>
                      <th>Valor Imp</th>
                      <th>Valor Mensal</th>
                      <th>Status</th>
					  <th>Ação</th>	
                      <th>View</th>
                     <!-- <th width="10%">Editar</th>-->
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Cliente</th>
                      <th>Serviço</th>
                      <th>Valor Imp</th>
                      <th>Valor Mensal</th>
                      <th>Status</th>
                      <th>Ação</th>
                      <th>View</th>
                     <!-- <th width="10%">Editar</th>-->
                    </tr>
                  </tfoot>
                  <tbody>
						<?php
						while($row = mysqli_fetch_array($res)) { 
							if($row['valor_imp'] == 0){
								$valor_imp 		= $row['valor_imp'];
								$valor_mensal 	= $row['valor_mensal'] - $row['desconto'];
							}else{
								$valor_mensal 	= $row['valor_mensal'];
								$valor_imp 		= $row['valor_imp'] - $row['desconto'];
							}

							$color_status = $row['color'];
							$nome_cliente = $row['nome_cliente'];
							if(strlen($row['nome_cliente']) < 2){
								$nome_cliente = $row['nome_empresa'];
							}
							?>
							<tr>
								<td><?php echo $nome_cliente;?></td>
								<td><?php echo $row['nome_servico'];?></td>
                                <td><?php echo "R$ ".number_format($valor_imp, 2, '.', '');?></td>
                                <td><?php echo "R$ ".number_format($valor_mensal, 2, '.', '');?></td>
								<td style="text-align-last: center;vertical-align: middle;" >
                                    <span  ondblclick="AlteraStatusOrc(<?php echo $row['id'];?>)" class="status" style="border-radius: 10px;cursor: pointer;background: <?php echo $color_status;?>"><?php echo $row['status_nome'];?></span>
                                    </td>
								<?php if($row['id_cliente'] != NULL){	?>
									<?php if($row['id_status'] == 2){ 
											if($row['contratacao'] == 1){
												if(checkContrato($row['id']) == true){	?>
													<td>Gerado</td>
											<?php }	else{ ?>
												<td><button onclick="geraContrato(<?php echo $row['id'];?>)" class="btn btn-primary">Contrato</button></td>
											<?php }?>	
										<?php }else{?>
												<td>Serviço Avulso</td>
										<?php }?>
									<?php }else{ ?>
											<td></td>
									<?php } 
								}else{?>
								<td><button onclick="geraCliente(<?php echo $row['id'];?>,'<?php echo $nome_cliente;?>','<?php echo $row['responsavel'];?>')" class="btn btn-primary">Novo Cliente</button></td>
								<?php }?>
								<td><center><a class="btn btn-primary btn-circle" target="_BLANK" href="views/view_orc.php?id=<?php echo $row['id'];?>" ><i class="fas fa-eye" ></i></a></center></td>
							</tr>
						<?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>		  
	
		 
		<!-- AddOrcamento -->
		<div class="modal fade" style="top:25%" id="AlteraStatusOrc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Status Orçamento</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
					</div>
					<div class="modal-body">
						<form action="php/altera_status_orcamento.php" method="post">
							<input type="hidden" class="form-control" name="id_orcamento_status" id="id_orcamento_status" />
							<select name="id_status_orcamento" class="form-control" >
								<?php 
									$sql = "select * from status where tipo = 'ORCAMENTO'";
									$res = mysqli_query($conn,$sql);
									while($row = mysqli_fetch_array($res)){ ?>
										<option value="<?php echo $row['id'];?>"><?php echo $row['status'];?></option>
									<?php }?>		
							</select><br>
							<input type="submit" value="Alterar" class="btn btn-success" />
						</form>
					</div>
				</div>
			</div>
		</div>

		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                });
            });

			function AlteraStatusOrc(id){
				$('#AlteraStatusOrc').modal('show');
				$('#id_orcamento_status').val(id);
			}
			function edit(id){	
				$.get( "php/get_produto.php?id_produto="+id, function( data ) {
					 var json = JSON.parse(data);
					$( "#nome_edit" ).val( json[0].nome );
					$( "#preco_edit" ).val( json[1].preco );
					$( "#id_produto_edit" ).val( json[2].id );
					$( "#desc_edit" ).val( json[3].desc );
					$( "#marca_edit" ).val( json[4].id_marca );
					$( "#marca_edit" ).html( json[5].nome_marca );
					$( "#categoria_edit" ).val( json[6].id_categoria );
					$( "#categoria_edit" ).html( json[7].nome_categoria );
					$( "#ref_edit" ).val( json[8].ref );
					$( "#promocao_edit" ).val( json[9].promocao );
					
					$('#EditProd').modal('show');
					
				});
				
				$.get( "php/get_images.php?id_produto="+id+"&exclui=false&ima_id=", function( data ) {
					$( "#images" ).html( data );
				});
			}
			
			function exclui(id,id_ima){
				$.get( "php/get_images.php?id_produto="+id+"&ima_id="+id_ima+"&exclui=true", function( data ) {
					$( "#images" ).html( data );
				});
			}
		
			function altera_status(id){
				if($('#onoff'+id).is(':checked')){
					var status = 1;
					$.get("php/altera_status.php?id_produto="+id+"&status="+status, function (data){
						
					});
				}else{
					var status = 0;
					$.get("php/altera_status.php?id_produto="+id+"&status="+status, function (data){
						
					});
				}
            }
            
			function geraContrato(id_orcamento){
				$.get("php/get_orcamento.php?id_orcamento="+id_orcamento, function (data){
					
					var json = JSON.parse(data);
					$("#id_cliente_con").val(json[0].id_cliente);
					$("#id_cliente_con").html(json[1].nome_cliente);
					$("#id_servico_con").val(json[2].id_servico);
					$("#id_servico_con").html(json[3].nome_servico);
					$("#desconto_pagamento_con").val(json[4].desconto);
					$("#id_orcamento_con").val(json[5].id_orcamento);
					$("#AddContrato").modal('show');
				});
			}

			function geraCliente(id_orcamento,nome_cliente,responsavel){
				$("#razao").val(nome_cliente);
				$("#id_orcamento_cli").val(id_orcamento);
				$("#responsavel").val(responsavel);
				$("#AddCli").modal('show');
			}
           
		</script>