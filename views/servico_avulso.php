<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}


$sql = "SELECT 
            c.id,
            cli.razao_social,
            s.nome as servico,
            c.id_servico as servico_c,
            (s.valor - c.desconto) as valor,
            c.primeiro_pagamento,
            st.status as status_nome,
            st.id as id_status,
            st.color,
			c.valor_hora,
			c.qtd_hora
        FROM 
            sem_contrato as c 
            inner join cliente as cli ON
            c.id_cliente = cli.id
            left join servico as s ON
            c.id_servico = s.id
            inner join status as st on
            c.id_status = st.id
		";
$res = mysqli_query($conn,$sql);



	
?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Listagem de Serviços Avulsos
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddServicoAvulso" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Razão Social</th>
                      <th>Serviço</th>
                      <th>Vencimento</th>
                      <th>Valor</th>
                      <th>Status</th>
                     <!-- <th width="10%">Editar</th>-->
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Razão Social</th>
                      <th>Serviço</th>
                      <th>Vencimento</th>
                      <th>Valor</th>
                      <th>Status</th>
                     <!-- <th width="10%">Editar</th>-->
                    </tr>
                  </tfoot>
                  <tbody>
						<?php
						while($row = mysqli_fetch_array($res)) { 
                            $data     = date('d/m/Y',strtotime($row['primeiro_pagamento']));

							$color_status = $row['color'];
							$valor = $row['valor'];
							$servico = $row['servico'];


							if($row['id_status'] == 7){
								$click = "'OK'";
							}else{
								$click = $row['id'];
							}

							if($row['servico_c'] == 99){
								$servico = "Serviço Por Hora";
								$valor = $row['qtd_hora'] * $row['valor_hora'];
							}


							?>
							<tr>
								<td><?php echo $row['razao_social'];?></td>
								<td><?php echo $servico;?></td>
								<td><?php echo $data;?></td>
                                <td><?php echo "R$ ".number_format($valor, 2, '.', '');?></td>
								<td style="text-align-last: center;vertical-align: middle;" >
                                    <span ondblclick="AlteraStatusCont(<?php echo $click;?>)" class="status" style="background: <?php echo $color_status;?>"><?php echo $row['status_nome'];?></span>
                                    </td>
								
							</tr>
						<?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>		  
	
		
			<!-- AddOrcamento -->
		<div class="modal fade" style="top:25%" id="AlteraStatusCont" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Status Contrato</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
					</div>
					<div class="modal-body">
						<form action="php/altera_status_servico_avulso.php" method="post">
							<input type="hidden" class="form-control" name="id_sem_contrato_status" id="id_sem_contrato_status" />
							<select name="id_status_sem_contrato" class="form-control" >
								<?php 
									$sql = "select * from status where tipo = 'CONTRATO'";
									$res = mysqli_query($conn,$sql);
									while($row = mysqli_fetch_array($res)){ ?>
										<option value="<?php echo $row['id'];?>"><?php echo utf8_encode($row['status']);?></option>
									<?php }?>		
							</select><br>
							<input type="submit" value="Alterar" class="btn btn-success" />
						</form>
					</div>
				</div>
			</div>
		</div>

		 
		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                });
            });
			function AlteraStatusCont(id){
				if(id != 'OK'){
					$('#AlteraStatusCont').modal('show');
					$('#id_sem_contrato_status').val(id);	
				}else{
					alert('Contrato em processo, só pode ser alterado com medidas judiciais');
				}
			}
			function edit(id){	
				$.get( "php/get_produto.php?id_produto="+id, function( data ) {
					 var json = JSON.parse(data);
					$( "#nome_edit" ).val( json[0].nome );
					$( "#preco_edit" ).val( json[1].preco );
					$( "#id_produto_edit" ).val( json[2].id );
					$( "#desc_edit" ).val( json[3].desc );
					$( "#marca_edit" ).val( json[4].id_marca );
					$( "#marca_edit" ).html( json[5].nome_marca );
					$( "#categoria_edit" ).val( json[6].id_categoria );
					$( "#categoria_edit" ).html( json[7].nome_categoria );
					$( "#ref_edit" ).val( json[8].ref );
					$( "#promocao_edit" ).val( json[9].promocao );
					
					$('#EditProd').modal('show');
					
				});
				
				$.get( "php/get_images.php?id_produto="+id+"&exclui=false&ima_id=", function( data ) {
					$( "#images" ).html( data );
				});
			}
			
			function exclui(id,id_ima){
				$.get( "php/get_images.php?id_produto="+id+"&ima_id="+id_ima+"&exclui=true", function( data ) {
					$( "#images" ).html( data );
				});
			}
		
			function altera_status(id){
				if($('#onoff'+id).is(':checked')){
					var status = 1;
					$.get("php/altera_status.php?id_produto="+id+"&status="+status, function (data){
						
					});
				}else{
					var status = 0;
					$.get("php/altera_status.php?id_produto="+id+"&status="+status, function (data){
						
					});
				}
            }
            

           
		</script>