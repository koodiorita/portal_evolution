<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['user_id'])){
	$usuario_id = $_SESSION['user_id'];
}else{
	header('Location: login.php');
}


$sql = "select * from fornecedor
		";
$res = mysqli_query($conn,$sql);



	
?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Fornecedores
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddFor" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Endereço</th>
                      <th>Telefone</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nome</th>
                      <th>Endereço</th>
                      <th>Telefone</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </tfoot>
                  <tbody>
						<?php
							
						while($row = mysqli_fetch_array($res)) { 
                            
							?>
							<tr>
								<td><?php echo $row['nome'];?></td>
								<td><?php echo $row['endereco'].", ".$row['numero']." - ".$row['bairro']." ". $row['cidade'];?></td>
								<td><?php echo $row['telefone'];?></td>
								<td><center><button class="btn btn-warning btn-circle" onclick="edit_fornecedor(<?php echo $row['id'];?>)" ><i class="fas fa-edit" ></i></button></center></td>
							</tr>
						<?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>

    </div>
	
	<div class="modal fade" id="EditFornecedor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document" style="max-width: 1000px;">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Fornecedor</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dados</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Financeiro</a>
							</li>
						<!--	<li class="nav-item">
								<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"></a>
							</li>-->
						</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
									
								<div id="conteudo-dados">
								
								</div>
							</div>
							<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
								<div id="conteudo-financeiro" class="divFinanceiro"></div>
							</div>
							<!--<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>-->
						</div>
					</div>
				  </div>
			</div>
	</div>
			<!-- AddProd 
			<div class="modal fade" id="EditFor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar de Fornecedor</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					
				</div>
				  </div>
			</div>
		  </div>
		-->
		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                });
            });
                    
		
		function edit_fornecedor(id){
			var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 50%;margin-top: 10%;margin-bottom: 10%'><span class='sr-only'>Loading...</span></div>";
			$("#conteudo-dados").html(data);
			$("#conteudo-financeiro").html(data);

			$.get( "php/dados_fornecedor.php?id="+id, function( data ) {
				$("#conteudo-dados").html(data);
			});

			$.get( "php/financeiro_fornecedor.php?id="+id, function( data ) {
				$("#conteudo-financeiro").html(data);
			});

			$('#EditFornecedor').modal('show');
		}
		</script>

<script>
        $(document).ready(function () {
            $("#buscaCepFor_edit").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_for_edit").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_for_edit").val(dados.logradouro);
                            $("#endereco_for_edit").css("background","#eee");
                            $("#bairro_for_edit").val(dados.bairro);
                            $("#bairro_for_edit").css("background","#eee");
                            $("#cidade_for_edit").val(dados.localidade);
                            $("#cidade_for_edit").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });
        </script>